module "bigquery" {
  source  = "gitlab.com/picnic-app/bigquery/google"
  version = "~> 1.0"

  env_id       = var.env_id
  service_name = var.service_name
  sa_email     = module.workload-identity.service_account.email

  tables = {
    classified-posts = {
      schema = [
        {
          name = "PostID"
          type = "STRING"
          mode = "REQUIRED"
        },
        {
          name = "PostType"
          type = "STRING"
        },
        {
          name = "ClassificationResults"
          type = "RECORD",
          mode = "REPEATED",
          fields = [
                {
                    name = "Class",
                    type = "STRING",
                }
          ]
        },
        {
          name = "SentimentAnalysisResults"
          type = "RECORD",
          mode = "REPEATED",
          fields = [
                {
                    name = "Name",
                    type = "STRING",
                },
                {
                    name = "Type",
                    type = "STRING",
                },     
                {
                    name = "Sentiment",
                    type = "FLOAT64",
                },                 
                {
                    name = "Magnitude",
                    type = "FLOAT64",
                },                 
                {
                    name = "Importance",
                    type = "FLOAT64",
                }                   
            ]
        },
        {
          name = "EventTimestamp"
          type = "TIMESTAMP"
        },
      ]
    }
  }
}
