data "google_storage_bucket" "public" {
  count = local.is_not_dev ? 1 : 0

  name = var.env_id == "prod" ? "picnic-storage-api-public-prod-us" : "picnic-storage-api-public-stg-eu"
}

// Video Intelligence API uses this SA to access bucket. And for some reason it cannot access public objects
// in bucket without explicitly granted permissions.
resource "google_storage_bucket_iam_member" "public" {
  count = local.is_not_dev ? 1 : 0

  bucket = one(data.google_storage_bucket.public[*]).name
  member = "serviceAccount:${module.workload-identity.service_account.email}"
  role   = "roles/storage.objectViewer"
}
