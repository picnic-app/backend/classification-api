package main

import (
	"context"
	"io"
	"log"
	"net/http"
	"os"
	"syscall"
	"time"

	"go.uber.org/zap"

	"gitlab.com/picnic-app/backend/classification-api/internal/controller"
	"gitlab.com/picnic-app/backend/classification-api/internal/repository/bigquery"
	"gitlab.com/picnic-app/backend/classification-api/internal/service"
	"gitlab.com/picnic-app/backend/classification-api/internal/util/grpcclient"
	"gitlab.com/picnic-app/backend/libs/golang/config"
	"gitlab.com/picnic-app/backend/libs/golang/core"
	"gitlab.com/picnic-app/backend/libs/golang/core/mw"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/libs/golang/graceful"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
	"gitlab.com/picnic-app/backend/libs/golang/monitoring/monitoring"
	"gitlab.com/picnic-app/backend/libs/golang/monitoring/tracing"
	contentV1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/content-api/content/v1"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	app, err := initApp(ctx)
	if err != nil {
		log.Fatal(err)
	}
	cfg := app.Config()

	err = tracing.SetupExporter(ctx)
	if err != nil {
		logger.Errorf(ctx, "failed to set up tracing exporter: %v", err)
	}

	monitoring.RegisterPrometheusSuffix()

	debugSrv, err := app.RunDebug(ctx)
	if err != nil {
		logger.Fatalf(ctx, "failed to start debug server: %v", err)
	}

	if mux, ok := debugSrv.Handler.(*http.ServeMux); ok {
		mux.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
			body, err_ := io.ReadAll(r.Body)
			if err_ != nil {
				logger.Errorf(ctx, "failed to read body: %v", err_)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			_ = r.Body.Close()
			logger.InfoKV(ctx, "test request", zap.ByteString("body", body), zap.Error(err_))
		})
	}

	ctrl, cleanup, err := initController(ctx, cfg)
	if err != nil {
		logger.Fatalf(ctx, "failed to init controller: %v", err)
	}

	defer cleanup()

	gracefulShutdown := graceful.New(
		&graceful.ShutdownManagerOptions{Timeout: 60 * time.Second},
		graceful.Parallel(
			&graceful.ParallelShutdownOptions{
				Name:    "servers",
				Timeout: 30 * time.Second,
			},
			graceful.ShutdownErrorFunc(func() error {
				app.Close()
				return nil
			}),
			graceful.HTTPServer(debugSrv),
		),
		graceful.Context(cancel),
		graceful.Parallel(
			&graceful.ParallelShutdownOptions{
				Name:    "clients",
				Timeout: 30 * time.Second,
			},
			graceful.ShutdownErrorFunc(func() error {
				cleanup()
				return nil
			}),
			graceful.Tracer(),
		),
		graceful.Logger(nil),
	)
	gracefulShutdown.RegisterSignals(os.Interrupt, syscall.SIGTERM)
	defer func() {
		_ = gracefulShutdown.Shutdown(context.Background())
	}()

	err = app.Run(ctx, ctrl)
	if err != nil {
		logger.Fatal(ctx, err)
	}

	logger.Info(ctx, "gRPC server closed gracefully")
}

func initApp(ctx context.Context) (core.Application, error) {
	app, err := core.InitApp(ctx)
	if err != nil {
		return nil, err
	}

	ctxInterceptor := mw.NewServerContextInterceptor(config.String("env.auth.secret"))
	logInterceptor := mw.NewLogInterceptor()
	errInterceptor := mw.NewErrInterceptor()

	app = app.WithMW(ctxInterceptor).WithMW(logInterceptor).WithMW(errInterceptor)

	return app, nil
}

func initController(ctx context.Context, cfg config.Config) (controller.Controller, func(), error) {
	db, err := bigquery.NewBigQueryClient(ctx, cfg.BigQuery.ADCPath)
	if err != nil {
		logger.Fatalf(ctx, "failed to create bigquery client: %v", err)
	}

	eventbusClient, err := eventbus.DefaultClient(ctx)
	if err != nil {
		logger.Fatal(ctx, err)
	}

	// content-api client
	contentConn := grpcclient.New(
		ctx,
		config.String("env.svc.content.host"),
		config.Int("env.svc.content.port"),
		config.Bool("env.svc.content.secure"),
		config.String("env.service.name"),
		config.String("env.auth.secret"),
	)

	contentClient := contentV1.NewContentAPIClient(contentConn)

	svc := service.New(
		contentClient,
		eventbusClient,
		db,
	)
	if err != nil {
		logger.Fatalf(ctx, "failed to create the service: %v", err)
	}

	err = svc.Subscribe(ctx, eventbusClient)
	if err != nil {
		logger.Fatal(ctx, err)
	}

	return controller.New(svc), func() {
		eventbusClient.Close()
		_ = contentConn.Close()
	}, err
}
