package controller

import (
	"google.golang.org/grpc"

	"gitlab.com/picnic-app/backend/classification-api/internal/service"
)

func New(svc *service.Service) Controller {
	return Controller{
		service: svc,
	}
}

type Controller struct {
	service *service.Service
}

func (c Controller) Register(_ grpc.ServiceRegistrar) {
}
