package model

import "time"

type Classified struct {
	EventTimestamp           time.Time              `bigquery:"EventTimestamp"`
	PostID                   string                 `bigquery:"PostID"`
	PostType                 string                 `bigquery:"PostType"`
	ClassificationResults    []ClassificationEntity `bigquery:"ClassificationResults"`
	SentimentAnalysisResults []SentimentEntity      `bigquery:"SentimentAnalysisResults"`
}

type SentimentEntity struct {
	Name       string  `bigquery:"Name"`
	Type       string  `bigquery:"Type"`
	Sentiment  float32 `bigquery:"Sentiment"`
	Magnitude  float32 `bigquery:"Magnitude"`
	Importance float32 `bigquery:"Importance"`
}

type ClassificationEntity struct {
	Class string `bigquery:"Class"`
}
