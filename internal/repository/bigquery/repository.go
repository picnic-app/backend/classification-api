package bigquery

import (
	"context"

	"cloud.google.com/go/bigquery"
	"google.golang.org/api/option"
)

// NewBigQueryClient ...
func NewBigQueryClient(ctx context.Context, adcCredPath string) (*bigquery.Client, error) {
	var opts []option.ClientOption
	if adcCredPath != "" {
		opts = append(opts, option.WithCredentialsFile(adcCredPath))
	}
	client, err := bigquery.NewClient(ctx, bigquery.DetectProjectID, opts...)
	if err != nil {
		return nil, err
	}

	return client, nil
}
