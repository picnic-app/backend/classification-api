package repository

import (
	"context"
	"time"
)

type ReadActions interface{}

type WriteActions interface{}

type ReadWriteActions interface {
	ReadActions
	WriteActions
}

type Repository interface {
	SingleRead() ReadActions
	SingleWrite() WriteActions
	ReadOnlyTx(context.Context, func(ctx context.Context, tx ReadActions) error) error
	ReadWriteTx(context.Context, func(ctx context.Context, tx ReadWriteActions) error) (time.Time, error)
}
