package service

import (
	"context"
	"encoding/json"
	"time"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/picnic-app/backend/classification-api/internal/model"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
	contentV1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/content-api/content/v1"
)

func HandlePayload[M any](ctx context.Context, evt *event.Event, f func(ctx context.Context, payload M) error) error {
	b, err := json.Marshal(evt.Payload)
	if err != nil {
		return errors.Wrap(err, "marshalling to json")
	}

	var payload M
	if err = json.Unmarshal(b, &payload); err != nil {
		return errors.Wrap(err, "unmarshalling from json")
	}

	return f(ctx, payload)
}

func (s *Service) Subscribe(ctx context.Context, client eventbus.Client) error {
	err := client.Sub(ctx, eventbus.TopicID().Content, s.HandleContentEvent)
	if err != nil {
		return errors.Wrap(err, "subscription to content events")
	}

	return nil
}

func (s *Service) publishClassificationEvent(
	ctx context.Context,
	client eventbus.Client,
	classification model.Classified,
) error {
	if client == nil {
		return nil
	}

	var tags []string
	for _, tag := range classification.ClassificationResults {
		tags = append(tags, tag.Class)
	}

	tags = unique(tags)

	if len(tags) == 0 {
		tags = append(tags, "Unknown")
	}

	event := event.NewEvent(event.PostClassified, &event.PostClassifiedPayload{
		PostID:             classification.PostID,
		PostType:           classification.PostType,
		ClassificationTags: tags,
	})

	err := client.Topic(eventbus.TopicID().Classification).Pub(ctx, event)
	if err != nil {
		logger.Errorf(ctx, "publishClassificationEvent: failed with: %v for postID %v", err, classification.PostID)
		return err
	}

	return nil
}

func unique(tags []string) []string {
	unique := make(map[string]bool)
	for _, tag := range tags {
		unique[tag] = true
	}

	var result []string
	for tag := range unique {
		result = append(result, tag)
	}

	return result
}

func (s *Service) HandleContentEvent(ctx context.Context, evt *event.Event) error {
	ctx = auth.AddServiceNameToCtx(ctx, "content-api")
	ctx = auth.ToAdminCtx(ctx)
	// Adding a user ID to the context is required for the content API
	ctx = auth.AddUserIDToCtx(ctx, uuid.NewString())

	timestamp := time.Now().UTC()

	switch evt.EvtType {
	case event.TypeNewPost:
		return HandlePayload(ctx, evt, func(ctx context.Context, p event.NewPostPayload) error {
			logger.Debugf(ctx, "Handling new post event: %v", p)

			resp, err := s.contentClient.GetPost(ctx, &contentV1.GetPostRequest{
				Id:            p.PostID,
				ReturnDeleted: true,
			})

			if err != nil {
				if status.Code(err) != codes.NotFound {
					return errors.Wrap(err, "getting post")
				} else {
					logger.Infof(ctx, "Post not found, skipping: %v", p.PostID)
					return nil
				}
			}

			if resp != nil && resp.Post != nil {
				switch p.Type {
				case 1:
					data := resp.Post.GetData()
					text := data.GetTxt()
					tags, err := s.ClassifyText(ctx, text.Content)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to classify text in post",
							zap.String("postID", p.PostID),
							zap.Error(err),
							zap.String("text", text.Content),
						)

						return nil
					}

					tags = unique(tags)

					results, err := s.analyzeEntitySentiment(ctx, text.Content)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to analyze entities in post",
							zap.String("postID", p.PostID),
							zap.Error(err),
							zap.String("text", text.Content),
						)

						return nil
					}

					var sentimentEntities []model.SentimentEntity

					for _, entity := range results.Entities {
						sentimentEntities = append(sentimentEntities, model.SentimentEntity{
							Name:       entity.GetName(),
							Type:       entity.GetType().String(),
							Importance: entity.GetSalience(),
							Sentiment:  entity.GetSentiment().Score,
							Magnitude:  entity.GetSentiment().Magnitude,
						})
					}

					logger.Infof(ctx, "Found entities: %v", sentimentEntities)

					if len(tags) == 0 {
						tags = append(tags, "Unknown")
					}

					logger.Infof(ctx, "Classified text post, found tags: %v", tags)

					var classes []model.ClassificationEntity
					for _, tag := range tags {
						classes = append(classes, model.ClassificationEntity{
							Class: tag,
						})
					}

					classification := model.Classified{
						EventTimestamp:           timestamp,
						PostID:                   p.PostID,
						PostType:                 "text",
						ClassificationResults:    classes,
						SentimentAnalysisResults: sentimentEntities,
					}

					err = s.publishClassificationEvent(ctx, s.eventbusClient, classification)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to publish classification event",
							zap.Error(err),
						)

						return nil
					}

					err = s.SaveClassificationResults(ctx, classification)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to save classification results",
							zap.Error(err),
						)

						return nil
					}
					return nil
				case 2:
					data := resp.Post.GetData()
					image := data.GetImg()
					logger.Infof(ctx, "image url: %v", image.Url)

					imgTags, err := s.ClassifyImage(ctx, image.Url)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to classify image in post",
							zap.String("postID", p.PostID),
							zap.Error(err),
							zap.String("image_url", image.Url),
						)

						return nil
					}

					captionTags, err := s.ClassifyText(ctx, image.Content)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to classify text in post",
							zap.String("postID", p.PostID),
							zap.Error(err),
							zap.String("text", image.Content),
						)

						return nil
					}

					tags := append(imgTags, captionTags...)
					tags = unique(tags)

					if len(tags) == 0 {
						tags = append(tags, "Unknown")
					}

					logger.Infof(ctx, "Classified image post, found tags: %v", tags)

					var classes []model.ClassificationEntity
					for _, tag := range tags {
						classes = append(classes, model.ClassificationEntity{
							Class: tag,
						})
					}

					classification := model.Classified{
						EventTimestamp:           timestamp,
						PostID:                   p.PostID,
						PostType:                 "image",
						ClassificationResults:    classes,
						SentimentAnalysisResults: []model.SentimentEntity{},
					}

					err = s.publishClassificationEvent(ctx, s.eventbusClient, classification)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to publish classification event",
							zap.Error(err),
						)
					}

					err = s.SaveClassificationResults(ctx, classification)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to save classification results",
							zap.Error(err),
						)

						return nil
					}
					return nil
				case 3:
					logger.Infof(ctx, "Ignoring link post")
				case 4:
					data := resp.Post.GetData()
					video := data.GetVideo()
					logger.Infof(ctx, "video url: %v", video.Url)

					videoTags, err := s.ClassifyVideo(ctx, video.Url)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to classify video in post",
							zap.String("postID", p.PostID),
							zap.Error(err),
							zap.String("video_url", video.Url),
						)

						return nil
					}

					captionTags, err := s.ClassifyText(ctx, video.Content)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to classify text in post",
							zap.String("postID", p.PostID),
							zap.Error(err),
							zap.String("text", video.Content),
						)

						return nil
					}

					tags := append(videoTags, captionTags...)
					tags = unique(tags)

					if len(tags) == 0 {
						tags = append(tags, "Unknown")
					}

					logger.Infof(ctx, "Classified video post, found tags: %v", tags)

					var classes []model.ClassificationEntity
					for _, tag := range tags {
						classes = append(classes, model.ClassificationEntity{
							Class: tag,
						})
					}

					classification := model.Classified{
						EventTimestamp:           timestamp,
						PostID:                   p.PostID,
						PostType:                 "video",
						ClassificationResults:    classes,
						SentimentAnalysisResults: []model.SentimentEntity{},
					}

					err = s.publishClassificationEvent(ctx, s.eventbusClient, classification)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to publish classification event",
							zap.Error(err),
						)

						return nil
					}

					err = s.SaveClassificationResults(ctx, classification)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to save classification results",
							zap.Error(err),
						)

						return nil
					}
					return nil
				case 5:
					data := resp.Post.GetData()
					pollVariants := data.GetPoll()

					var tags []string

					for _, variant := range pollVariants.Variants {
						imgTags, err := s.ClassifyImage(ctx, variant.ImageUrl)
						if err != nil {
							logger.ErrorKV(ctx, "Failed to classify image in poll post",
								zap.String("postID", p.PostID),
								zap.Error(err),
								zap.String("varaint_image_url", variant.ImageUrl),
							)

							return nil
						}
						tags = append(tags, imgTags...)
					}

					captionTags, err := s.ClassifyText(ctx, pollVariants.Content)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to classify text in poll post",
							zap.String("postID", p.PostID),
							zap.Error(err),
							zap.String("text", pollVariants.Content),
						)

						return nil
					}

					tags = append(tags, captionTags...)
					tags = unique(tags)

					if len(tags) == 0 {
						tags = append(tags, "Unknown")
					}

					logger.Infof(ctx, "Classified poll post, found tags: %v", tags)

					var classes []model.ClassificationEntity
					for _, tag := range tags {
						classes = append(classes, model.ClassificationEntity{
							Class: tag,
						})
					}

					classification := model.Classified{
						EventTimestamp:           timestamp,
						PostID:                   p.PostID,
						PostType:                 "poll",
						ClassificationResults:    classes,
						SentimentAnalysisResults: []model.SentimentEntity{},
					}

					err = s.publishClassificationEvent(ctx, s.eventbusClient, classification)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to publish classification event",
							zap.Error(err),
						)

						return nil
					}

					err = s.SaveClassificationResults(ctx, classification)
					if err != nil {
						logger.ErrorKV(ctx, "Failed to save classification results",
							zap.Error(err),
						)

						return nil
					}
					return nil

				}
			}
			return nil
		})
	default:
		return nil
	}
}
