package service

import (
	"context"
	"strings"

	language "cloud.google.com/go/language/apiv1"
	"cloud.google.com/go/language/apiv1/languagepb"
	video "cloud.google.com/go/videointelligence/apiv1"
	videopb "cloud.google.com/go/videointelligence/apiv1/videointelligencepb"
	vision "cloud.google.com/go/vision/apiv1"
	"cloud.google.com/go/vision/v2/apiv1/visionpb"

	"cloud.google.com/go/bigquery"
	"gitlab.com/picnic-app/backend/classification-api/internal/model"
	"gitlab.com/picnic-app/backend/libs/golang/config"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	contentV1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/content-api/content/v1"
)

func New(
	contentClient contentV1.ContentAPIClient,
	eventbusClient eventbus.Client,
	bigQuerytClient *bigquery.Client,
) *Service {
	// Create dataset name
	dataset := config.ServiceName() + "_" + config.EnvID()
	dataset = strings.ReplaceAll(dataset, "-", "_")

	return &Service{
		bucket:          config.String("env.bucket.name"),
		region:          config.String("env.region"),
		contentClient:   contentClient,
		eventbusClient:  eventbusClient,
		bigQuerytClient: bigQuerytClient,
		datesetID:       dataset,
	}
}

type Service struct {
	bucket          string
	region          string
	contentClient   contentV1.ContentAPIClient
	eventbusClient  eventbus.Client
	bigQuerytClient *bigquery.Client
	datesetID       string
}

// SaveClassificationResults ...
func (s *Service) SaveClassificationResults(
	ctx context.Context,
	recordToSave model.Classified,
) error {
	client := s.bigQuerytClient

	inserter := client.Dataset(s.datesetID).Table("classified-posts").Inserter()

	if err := inserter.Put(ctx, recordToSave); err != nil {
		return err
	}
	return nil
}

func (s *Service) analyzeEntitySentiment(
	ctx context.Context,
	content string,
) (*languagepb.AnalyzeEntitySentimentResponse, error) {
	client, err := language.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	return client.AnalyzeEntitySentiment(ctx, &languagepb.AnalyzeEntitySentimentRequest{
		Document: &languagepb.Document{
			Source: &languagepb.Document_Content{
				Content: content,
			},
			Type: languagepb.Document_PLAIN_TEXT,
		},
	})
}

func (s *Service) ClassifyText(ctx context.Context, content string) ([]string, error) {
	client, err := language.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	resp, err := client.ClassifyText(ctx, &languagepb.ClassifyTextRequest{
		Document: &languagepb.Document{
			Source: &languagepb.Document_Content{
				Content: content,
			},
			Type: languagepb.Document_PLAIN_TEXT,
		},
		ClassificationModelOptions: &languagepb.ClassificationModelOptions{
			ModelType: &languagepb.ClassificationModelOptions_V2Model_{
				V2Model: &languagepb.ClassificationModelOptions_V2Model{
					ContentCategoriesVersion: languagepb.ClassificationModelOptions_V2Model_V2,
				},
			},
		},
	})
	if err != nil {
		return nil, err
	}

	tags := []string{}

	for _, c := range resp.GetCategories() {
		if c.GetConfidence() > 0.5 {
			tags = append(tags, c.GetName())
		}
	}

	return tags, nil
}

func (s *Service) ClassifyImage(ctx context.Context, url string) ([]string, error) {
	client, err := vision.NewImageAnnotatorClient(ctx)
	if err != nil {
		return nil, err
	}

	image := vision.NewImageFromURI(s.bucket + url)
	annotations, err := client.LocalizeObjects(ctx, image, nil)
	if err != nil {
		return nil, err
	}

	tags := []string{}
	if len(annotations) == 0 {
		// No objects found.
		return tags, nil
	}

	// Objects:
	for _, annotation := range annotations {
		if annotation.Score > 0.5 {
			tags = append(tags, annotation.Name)
		} else {
			break
		}

	}

	// Explicit content:
	props, err := client.DetectSafeSearch(ctx, image, nil)
	if err != nil {
		return nil, err
	}

	if props.Adult == visionpb.Likelihood_VERY_LIKELY || props.Adult == visionpb.Likelihood_LIKELY {
		tags = append(tags, "adult")
	}

	if props.Medical == visionpb.Likelihood_VERY_LIKELY || props.Medical == visionpb.Likelihood_LIKELY {
		tags = append(tags, "medical")
	}

	if props.Racy == visionpb.Likelihood_VERY_LIKELY || props.Racy == visionpb.Likelihood_LIKELY {
		tags = append(tags, "racy")
	}

	if props.Spoof == visionpb.Likelihood_VERY_LIKELY || props.Spoof == visionpb.Likelihood_LIKELY {
		tags = append(tags, "spoof")
	}

	if props.Violence == visionpb.Likelihood_VERY_LIKELY || props.Violence == visionpb.Likelihood_LIKELY {
		tags = append(tags, "violence")
	}

	return tags, nil
}

func (s *Service) ClassifyVideo(ctx context.Context, url string) ([]string, error) {
	client, err := video.NewClient(ctx)
	if err != nil {
		return nil, err
	}
	defer client.Close()

	op, err := client.AnnotateVideo(ctx, &videopb.AnnotateVideoRequest{
		Features: []videopb.Feature{
			videopb.Feature_LABEL_DETECTION,
			videopb.Feature_EXPLICIT_CONTENT_DETECTION,
		},
		InputUri:   s.bucket + url,
		LocationId: s.region,
	})
	if err != nil {
		return nil, err
	}

	resp, err := op.Wait(ctx)
	if err != nil {
		return nil, err
	}
	tags := []string{}

	if len(resp.AnnotationResults) > 0 && resp.AnnotationResults[0].SegmentLabelAnnotations != nil {
		result := resp.AnnotationResults[0].SegmentLabelAnnotations

		for _, label := range result {
			for _, segment := range label.Segments {
				if segment.Confidence > 0.5 {
					tags = append(tags, label.Entity.Description)
				}
			}
		}
	}

	if len(resp.AnnotationResults) > 0 && resp.AnnotationResults[0].ExplicitAnnotation != nil {
		explicitCheck := resp.AnnotationResults[0].ExplicitAnnotation
		if explicitCheck.Frames != nil {
			for _, frame := range explicitCheck.Frames {
				if frame.PornographyLikelihood == videopb.Likelihood_VERY_LIKELY ||
					frame.PornographyLikelihood == videopb.Likelihood_LIKELY {
					explicitLikelihood := "PornographyLikelihood=" + frame.PornographyLikelihood.String()
					tags = append(tags, explicitLikelihood)
				}
			}
		}
	}

	return tags, nil
}
