package aes

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"fmt"
	"io"
)

type AESService struct {
	key []byte
}

func (s *AESService) Encrypt(plaintext []byte) ([]byte, error) {
	block, err := aes.NewCipher(s.key)
	if err != nil {
		return nil, err
	}

	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	initializationVector := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, initializationVector); err != nil {
		return nil, err
	}

	cipher.NewCFBEncrypter(block, initializationVector).XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	return ciphertext, nil
}

func (s *AESService) Decrypt(ciphertext []byte) ([]byte, error) {
	block, err := aes.NewCipher(s.key)
	if err != nil {
		return nil, err
	}

	if len(ciphertext) < aes.BlockSize {
		return nil, fmt.Errorf("ciphertext too short")
	}

	initializationVector := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	cipher.NewCFBDecrypter(block, initializationVector).XORKeyStream(ciphertext, ciphertext)

	return ciphertext, nil
}

// NewAESService secret and salt must be either 128 , 192 , 256 bit
func NewAESService(secret, salt string) AESService {
	secretByte := []byte(secret)
	saltByte := []byte(salt)

	saltedSecret := make([]byte, len(secretByte)+len(saltByte))
	copy(saltedSecret, secretByte)
	copy(saltedSecret[len(secretByte):], saltByte)

	return AESService{
		key: saltedSecret,
	}
}
