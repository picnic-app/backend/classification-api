package aes

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/picnic-app/backend/classification-api/internal/util/aes"
)

func TestAES(t *testing.T) {
	// nolint
	secret, salt := "B&E)H@McQfTjWnZr", "z$C&F)J@NcRfUjXn"

	aesService := aes.NewAESService(secret, salt)

	tests := []struct {
		name string
		text string
	}{
		{
			name: "encrypt-decrypt 5 characters",
			text: "User1",
		},

		{
			name: "encrypt-decrypt  10 characters",
			text: "User111111",
		},

		{
			name: "encrypt-decrypt  30 characters",
			text: "User11111111111111111111111111",
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			cipherText, err := aesService.Encrypt([]byte(tc.text))
			assert.NoError(t, err)
			plainText, err := aesService.Decrypt(cipherText)
			assert.NoError(t, err)

			assert.Equal(t, string(plainText), tc.text)
		})
	}
}
