package convert

import (
	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/libs/golang/cursor"
	profileV2 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/profile-api/profile/v2"
)

func ToPointer[T any](v T) *T {
	return &v
}

func ToPointers[T any](sl []T) []*T {
	var res []*T
	for _, t := range sl {
		t := t
		res = append(res, &t)
	}

	return res
}

func ToValues[T any](sl []*T) []T {
	var res []T
	for _, t := range sl {
		res = append(res, *t)
	}

	return res
}

func ToNullString(text string) spanner.NullString {
	return spanner.NullString{StringVal: text, Valid: text != ""}
}

func ToCursor[M any](pageCursor *profileV2.PageCursor) (cursor.Cursor, error) {
	var m M
	return cursor.FromParams(m, &cursor.Params{
		ID:    pageCursor.GetLastId(),
		Dir:   int(pageCursor.GetDir()),
		Limit: pageCursor.GetLimit(),
	})
}

func ToPageInfo(in cursor.Page) *profileV2.PageInfo {
	res := &profileV2.PageInfo{
		HasPrev: in.HasPrev(),
		HasNext: in.HasNext(),
		Length:  in.Length(),
	}

	firstID := in.FirstID()
	if firstID != "" {
		res.FirstId = &firstID
	}

	lastID := in.LastID()
	if lastID != "" {
		res.LastId = &lastID
	}

	return res
}
