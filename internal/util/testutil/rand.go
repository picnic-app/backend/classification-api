package testutil

import (
	"strings"

	"github.com/google/uuid"
)

func RandEmail() string {
	return strings.ReplaceAll(uuid.New().String(), "-", "")[:25] + "@dummy.com"
}

func RandStr() string {
	return strings.ReplaceAll(uuid.New().String(), "-", "")[:25]
}

func RandStrPtr() *string {
	s := RandStr()

	return &s
}

func RandEmailPtr() *string {
	s := RandEmail()

	return &s
}
