package validation

import (
	"regexp"

	"github.com/forPelevin/gomoji"
	"github.com/pkg/errors"
)

func ValidateUsername(username string) error {
	if username == "" {
		return errors.New("empty username")
	}

	emojis := gomoji.CollectAll(username)
	text := gomoji.RemoveEmojis(username)
	if len(text)+len(emojis) > 30 {
		return errors.New("username can be max 30 chars")
	}

	r, err := regexp.Compile("^[a-z0-9_.]*$")
	if err != nil {
		return err
	}
	if !r.MatchString(text) {
		return errors.New("username only allows alpha-numeric characters")
	}

	return nil
}
